package model;

import java.io.Serializable;


/**
 * Donne les informations de la location
 */
public abstract class Location implements Serializable{
    public static int nbLocations = 0;
    protected int id;
    protected String dateDebut;
    protected String dateFin;
    protected Client client;
    protected Vehicule vehicule;

    /**
     * Crée une nouvelle location
     * @param dateDebut date de début de location
     * @param dateFin date de fin de location
     */
    public Location(String dateDebut, String dateFin, Client client, Vehicule vehicule) {
        this.id = Location.nbLocations;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.client = client;
        this.vehicule = vehicule;
        Location.nbLocations++;
    }

    /**
     * Récupère l'id de la location
     * @return id de la location
     */
    public int getId() {
        return id;
    }
    public String getDateDebut() {
        return dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public Client getClient() {
        return client;
    }

    public Vehicule getVehicule() {
        return vehicule;
    }
    
}
