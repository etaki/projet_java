package model;


public abstract class VehiculeTerrestre extends Vehicule{
    protected int kilometrage;
    protected int puissance;
    protected int forfaitKilometrique;
    VehiculeTerrestre(int id,String marque,String etat,int vitesseMax,String model,float prixJour,int nbPlaces,int kilometrage,int puissance,int forfaitKilometrique){
        super(id,marque,etat,vitesseMax,model,prixJour,nbPlaces);
        this.kilometrage = kilometrage;
        this.puissance = puissance;
        this.forfaitKilometrique = forfaitKilometrique;   
    }
    /**
     * Obtenir le kilometrage du vehicule terrestre
     * @return le kilometrage du vehicule terrestre
     */
    public int getKilometrage() {
        return kilometrage;
    }
    /**
     * Change le kilometrage du vehicule terrestre
     * @param kilometrage
     */
    public void setKilometrage(int kilometrage) {
        this.kilometrage = kilometrage;
    }
    /**
     * Obtenir la puissance du vehicule terrestre
     * @return la puissance du vehicule terrestre
     */
    public int getPuissance() {
        return puissance;
    }
    /**
     * Change la puissance du vehicule terrestre
     * @param puissance
     */
    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }
    /**
     * Obtenir le forfait kilometrique du vehicule terrestre 
     * @return le orfait kilometrique
     */
    public int getForfaitKilometrique() {
        return forfaitKilometrique;
    }
    /**
     * change le forfait kilometrique
     * @param forfaitKilometrique
     */
    public void setForfaitKilometrique(int forfaitKilometrique) {
        this.forfaitKilometrique = forfaitKilometrique;
    }
    
}
