package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;


/**
 * Représente une agence de location de véhicules
 */
public class Agence implements Serializable{
    private static int nbAgences = 0;
    private int id;
    private String nom;
    private HashMap<Integer, Client> clients = new HashMap<Integer, Client>();
    private HashMap<Integer, Location> locations = new HashMap<Integer, Location>();
    private HashMap<Integer, Vehicule> listeVehicules = new HashMap<Integer, Vehicule>();
    
    /**
     * Crée une nouvelle agence de location
     * @param id id de l'agence
     * @param nom nom de l'agence
     */
    public Agence(String nom) {
        this.id = nbAgences;
        this.nom = nom;
        
        nbAgences++;
        
    }

    /**
     * Récupère l'id de l'agence
     * @return id de l'agence
     */
    public int getId() {
        return id;
    }


    /**
     * Change l'id de l'agence
     * @param id id de l'agence
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Récupère le nom de l'agence
     * @return nom de l'agence
     */
    public String getNom() {
        return nom;
    }


    /**
     * Change le nom de l'agence
     * @param nom nom de l'agence
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Récupère la liste des clients
     * @return liste des clients
     */
    public HashMap<Integer, Client> getClients() {
        //Collection<Client> liste = this.clients.values(); 
        //return new ArrayList<>(liste);
        return this.clients;
    }
    
    /**
     * Ajoute un client à l'agence
     * @param client client à ajouter
     */
    public void addClient(Client client){
        this.clients.put(client.getId(), client);
    }
    
    /**
     * Récupère les informations d'un client à partir de son id
     * @param id id du client
     * @return le client
     */
    public Client getClient(int id){
        return (Client)clients.get(id);
    }

    /**
     * Supprime un client à partir de son id
     * @param id id du client
     */
    public void delClient(int id){
        clients.remove(id);
    }

    /**
     * Modifie un client déjà existant
     * @param id id du client
     * @param vehicule client qui remplace celui existant
     */
    public void modifClient(int id, Client client){
        clients.put(id, client);
    }

    /**
     * Récupère la liste des véhicules
     * @return liste des véhicules
     */
    public HashMap<Integer, Vehicule> getVehicules() {
       // Collection<Vehicule> liste = this.listeVehicules.values(); 
       // return new ArrayList<>(liste);
       return listeVehicules;
    }

    /**
     * Ajoute un véhicule à l'agence
     * @param vehicule véhicule à ajouter
     */
    public void addVehicule(Vehicule vehicule){
        this.listeVehicules.put(vehicule.getId(), vehicule);
    }
    
    /**
     * Récupère les informations d'un véhicule à partir de son id
     * @param id id du véhicule
     * @return le véhicule
     */
    public Vehicule getVehicule(int id){
        return listeVehicules.get(id);
    }

    /**
     * Modifie un véhicule déjà existant
     * @param id id du véhicule
     * @param vehicule véhicule qui remplace celui existant
     */
    public void modifVehicule(int id, Vehicule vehicule){
        listeVehicules.put(id, vehicule);
    }
    
    /**
     * Supprime un véhicule à partir de son id
     * @param id id du véhicule
     */
    public void delVehicule(int id){
        listeVehicules.remove(id);
        locations.remove(id);
    }

    /**
     * Récupère la liste des locations
     * @return liste des locations
     */
    public HashMap<Integer, Location> getLocations() {
        return this.locations;
    }

    /**
     * Ajoute une location à l'agence
     * @param location location à ajouter
     */
    public void addLocation(Location location){
        this.locations.put(location.getId(), location);
    }
    
    /**
     * Récupère les informations d'une location à partir de son id
     * @param id id de la location
     * @return la location
     */
    public Location getLocation(int id){
        return (Location)locations.get(id);
    }

    /**
     * Supprime une location à partir de son id
     * @param id id de la location
     */
    public void delLocation(int id){
        locations.remove(id);
    }

    /**
     * Modifie une location déjà existante
     * @param id id de la location
     * @param location location qui remplace celle existante
     */
    public void modifLocation(int id, Location location){
        locations.put(id, location);
    }

    /**
     * Sérialise l'agence dans un fichier .ag
     */
    public void serialiserAgence(){
        try {
            FileOutputStream fileOutput = new FileOutputStream("./model/agence" + id + ".ag");
            ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
            objectOutput.writeObject(this);
            fileOutput.close();
            objectOutput.close();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * @param fichier nom du fichier à désérialiser
     * @return l'agence
     */
    public void deserialiserAgence(String fichier){
        Agence agence = null;
        try {
            FileInputStream fileInput = new FileInputStream(fichier);
            ObjectInputStream objectInput = new ObjectInputStream(fileInput);
            agence = (Agence)(objectInput.readObject());
            objectInput.close();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        this.id = agence.id;
        this.nom = agence.nom;
        this.clients = agence.clients;
        this.locations = agence.locations;
        this.listeVehicules = agence.listeVehicules;
    }

    public static void main(String[] args) {
        Agence agence = new Agence("Alors oui");
        Client client = new Client("Nom", "Pren", "Ad", "10/10/2012");
        VehiculeAerien vehicule = new VehiculeAerien(0, "d", "d", 1, "model", 1.2f, 4, 1.5f, 6, "avion");
        agence.addClient(client);
        agence.addVehicule(vehicule);
        agence.addLocation(new LocationVehiculeAerien(client, vehicule, "ajd", "demain", 7f, 81f, 6.5f));
        agence.serialiserAgence();
        
        Agence agenceN = new Agence("nulle");
        agenceN.deserialiserAgence("./model/agence0.ag");
        System.out.println(agenceN.nom);
        
    }
}
