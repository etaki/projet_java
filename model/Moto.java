package model;


public class Moto extends VehiculeTerrestre{
    private boolean carenage;
    public Moto(int id,String marque,String etat,int vitesseMax,String model,float prixJour,int nbPlaces,int kilometrage,int puissance,int forfaitKilometrique,boolean carenage){
        super(id, marque, etat, vitesseMax, model, prixJour, nbPlaces, kilometrage, puissance, forfaitKilometrique);
        this.carenage = carenage;        
    }

    /**
     * Permet de savoir si la moto est avec carenage ou pas
     * @return si la moto est avec carenage ou pas
     */
    public boolean isCarenage() {
        return carenage;
    }
    /**
     * Change l'etat du carenage
     * @param carenage
     */
    public void setCarenage(boolean carenage) {
        this.carenage = carenage;
    }

    @Override
    public String toString() {
        return "Moto [id=" + id + ", carenage=" + carenage + ", marque=" + marque + ", kilometrage=" + kilometrage
                + ", etat=" + etat + ", puissance=" + puissance + ", vitesseMax=" + vitesseMax
                + ", forfaitKilometrique=" + forfaitKilometrique + ", model=" + model + ", prixJour=" + prixJour
                + ", nbPlaces=" + nbPlaces + "]";
    }

    
}
