package model;



/**
 * Donne des informations pour la location d'un véhicule terrestre
 */
public class LocationVehiculeTerrestre extends Location{
    private int kmPrevi;
    private float prixPrevi;
    private float tarif;

    /**
     * Crée une location pour un véhicule terrestre
     * @param dateDebut date de début de location
     * @param dateFin date de fin de location
     * @param kmPrevi nombre de km prévisonnels
     * @param prixPrevi prix prévisonnel pour la location
     * @param tarif prix par km
    */
    public LocationVehiculeTerrestre(Client client, Vehicule vehicule, String dateDebut, String dateFin, int kmPrevi, float prixPrevi, float tarif) {
        super(dateDebut, dateFin, client, vehicule);
        this.kmPrevi = kmPrevi;
        this.prixPrevi = prixPrevi;
        this.tarif = tarif;
    }

    /**
     * Récupère le nombre de km prévisonnels
     * @return nombre de km prévisonnels
     */
    public int getKmPrevi() {
        return kmPrevi;
    }

    /**
     * Change le nombre de km prévisonnels
     * @param kmPrevi nombre de km prévisonnels
     */
    public void setKmPrevi(int kmPrevi) {
        this.kmPrevi = kmPrevi;
    }

    /**
     * Récupère le prix prévisonnel pour la location
     * @return prix prévisonnel pour la location
     */
    public float getPrixPrevi() {
        return prixPrevi;
    }

    /**
     * Change le prix prévisonnel pour la location
     * @param prixPrevi prix prévisonnel pour la location
     */
    public void setPrixPrevi(float prixPrevi) {
        this.prixPrevi = prixPrevi;
    }

    /**
     * Récupère le prix par km
     * @return prix par km
     */
    public float getTarif() {
        return tarif;
    }

    /**
     * Change le prix par km
     * @param tarif prix par km
     */
    public void setTarif(float tarif) {
        this.tarif = tarif;
    }

    public float rendreVehicule(float nbKmReel){
        if(nbKmReel<51) return 25f;
        else if(nbKmReel>=51 && nbKmReel<=100) this.tarif = 0.5f;
        else if(nbKmReel>=101 && nbKmReel<=200) this.tarif = 0.3f;
        else if(nbKmReel>=201 && nbKmReel<=300) this.tarif = 0.2f;
        else this.tarif = 0.1f;

        return (nbKmReel - 50)*this.tarif + 375f ;
    }
    
}
