package model;



/**
 * Donne des informations pour la location d'un véhicule terrestre
 */
public class LocationVehiculeAerien extends Location{
    private float nbHVolPrevi;
    private float prixPrevi;
    private float tarif;
    
    
    /**
     * Crée une location pour un véhicule aérien
     * @param dateDebut date de début de location
     * @param dateFin date de fin de location
     * @param nbHVolPrevi nombre d'heures de vol prévisionnelles
     * @param prixPrevi prix prévisionnel pour la location
     * @param tarif prix par heure de vol
     */
    public LocationVehiculeAerien(Client client, Vehicule vehicule, String dateDebut, String dateFin, float nbHVolPrevi, float prixPrevi,
            float tarif) {
        super(dateDebut, dateFin, client, vehicule);
        this.nbHVolPrevi = nbHVolPrevi;
        this.prixPrevi = prixPrevi;
        this.tarif = tarif;
    }


    /**
     * Récupère le nombre d'heures de vol prévisionnelles
     * @return le nombre d'heures de vol prévisionnelles
     */
    public float getNbHVolPrevi() {
        return nbHVolPrevi;
    }


    /**
     * Change le nombre d'heures de vol prévisionnelles
     * @param nbHVolPrevi nombre d'heures de vol prévisionnelles
     */
    public void setNbHVolPrevi(float nbHVolPrevi) {
        this.nbHVolPrevi = nbHVolPrevi;
    }


    /** 
     * Récupère le prix prévisionnel pour la location
     * @return prix prévisionnel pour la location
     */
    public float getPrixPrevi() {
        return prixPrevi;
    }


    /** 
     * Change le prix prévisionnel pour la location
     * @param prixPrevi prix prévisionnel pour la location
     */
    public void setPrixPrevi(float prixPrevi) {
        this.prixPrevi = prixPrevi;
    }


    /**
     * Récupère le prix par heure de vol
     * @return prix par heure de vol
     */
    public float getTarif() {
        return tarif;
    }


    /** 
     * Change le prix par heure de vol
     * @param tarif prix par heure de vol
     */
    public void setTarif(float tarif) {
        this.tarif = tarif;
    }
    

    /**
     * Rend le véhicule loué et calcule le prix à payer
     * @param nbHVolPrevi nombre de dheures de vol réelles
     * @return prix de la location
     */
    public float rendreVehicule(float nbHVolPrevi){
        if(nbHVolPrevi<5) return 375f;
        else if(nbHVolPrevi>=5 && nbHVolPrevi<=6) this.tarif = 75f;
        else if(nbHVolPrevi>=6 && nbHVolPrevi<=10) this.tarif = 55f;
        else this.tarif = 45f;
        
        return (nbHVolPrevi - 5)*this.tarif + 375f ;
    }
}
