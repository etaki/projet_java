package model;

public class Voiture extends VehiculeTerrestre{
    private boolean toitOuvrant;
    public Voiture(int id,String marque,String etat,int vitesseMax,String model,float prixJour,int nbPlaces,int kilometrage,int puissance,int forfaitKilometrique,boolean toitOuvrant){
        super(id, marque, etat, vitesseMax, model, prixJour, nbPlaces, kilometrage, puissance, forfaitKilometrique);
        this.toitOuvrant = toitOuvrant;
    }
    /**
     * Savoir si le toit de la voiture est ouvrant
     * @return si le toit de la voiture est ouvrant
     */
    public boolean isToitOuvrant() {
        return toitOuvrant;
    }
    /**
     * change l'option du tiot ouvrant
     * @param toitOuvrant
     */
    public void setToitOuvrant(boolean toitOuvrant) {
        this.toitOuvrant = toitOuvrant;
    }
    @Override
    public String toString() {
        return "[id=" + id + ", toitOuvrant=" + toitOuvrant + ", marque=" + marque + ", kilometrage="
                + kilometrage + ", etat=" + etat + ", puissance=" + puissance + ", vitesseMax=" + vitesseMax
                + ", forfaitKilometrique=" + forfaitKilometrique + ", model=" + model + ", prixJour=" + prixJour
                + ", nbPlaces=" + nbPlaces +"]";
    }
    
    
}
