package model;

import java.io.Serializable;

public abstract class Vehicule  implements Serializable{
    protected int id;
    protected String marque;
    protected String etat;
    protected int vitesseMax;
    protected String model;
    protected float prixJour;
    protected int nbPlaces;
    public static int nbVehicules;

    public Vehicule(int id,String marque,String etat,int vitesseMax,String model,float prixJour,int nbPlaces){
        this.id =  id;
        this.marque =  marque;
        this.etat =  etat;
        this.vitesseMax =  vitesseMax;
        this.model =  model;
        this.prixJour =  prixJour;
        this.nbPlaces = nbPlaces;
        nbVehicules++;
    }
    /**
     * Obtenir l'id du vehicule
     * @return l'id du vehicule
     */
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    /**
     * Obtenir la marque du vehicule
     * @return la marque du vehicule
     */
    public String getMarque() {
        return marque;
    }
    public void setMarque(String marque) {
        this.marque = marque;
    }
    /**
     * Obtenir l'etat du vehicule
     * @return l'etat du vehicule
     */
    public String getEtat() {
        return etat;
    }
    public void setEtat(String etat) {
        this.etat = etat;
    }
    /**
     * Obtenir la vitesse max du vehiule
     * @return la vitesse max du vehicule
     */
    public int getVitesseMax() {
        return vitesseMax;
    }
    public void setVitesseMax(int vitesseMax) {
        this.vitesseMax = vitesseMax;
    }
    /**
     * Obtenir le modele du vehicule
     * @return le modele du vehicule
     */
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    /**
     * Obtenir le prix du vehicule par jour
     * @return le prix du vehiule par jour
     */
    public float getPrixJour() {
        return prixJour;
    }
    public void setPrixJour(float prixJour) {
        this.prixJour = prixJour;
    }
    /**
     * Obtenir le nb de places dans le vehicule
     * @return le nb de places dans le vehicule
     */
    public int getNbPlaces() {
        return nbPlaces;
    }
    public void setNbPlaces(int nbPlaces) {
        this.nbPlaces = nbPlaces;
    }
    @Override
    public String toString() {
        return "Vehicule [id=" + id + ", marque=" + marque + ", etat=" + etat + ", vitesseMax=" + vitesseMax
                + ", model=" + model + ", prixJour=" + prixJour + ", nbPlaces=" + nbPlaces + "]";
    }
}
