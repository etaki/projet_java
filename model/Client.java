package model;

import java.io.Serializable;


/**
 * Représente un client
 */
public class Client implements Serializable{
    public static int nbClients = 0;
    private int id;
    private String nom;
    private String prenom;
    private String adresse;
    private String clientDepuisLe;

    /**
     * Crée un nouveau client
     * @param nom nom du client
     * @param prenom prenom du client
     * @param adresse adresse du client
     * @param clientDepuisLe date depuis laquelle c'est un client
     */
    public Client(String nom, String prenom, String adresse, String clientDepuisLe) {
        this.id = Client.nbClients;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.clientDepuisLe = clientDepuisLe;
        Client.nbClients++;
    }

    /**
     * Récupère l'id du client
     * @return id du client
     */
    public int getId() {
        return id;
    }

    /**
     * Change l'id du client
     * @param id id du client
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Récupère le nom du client
     * @return nom du client
     */
    public String getNom() {
        return nom;
    }

    /**
     * Change le nom du client
     * @param nom nom du client
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Récupère le prenom du client
     * @return prenom du client
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Change le prenom du client
     * @param prenom prenom du client
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Récupère l'adresse du client
     * @return
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Change l'adresse du client
     * @param adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Récupère la date depuis laquelle c'est un client
     * @return
     */
    public String getClientDepuisLe() {
        return clientDepuisLe;
    }

    /**
     * Change la date depuis laquelle c'est un client
     * @param clientDepuisLe
     */
    public void setClientDepuisLe(String clientDepuisLe) {
        this.clientDepuisLe = clientDepuisLe;
    }

    @Override
    public String toString() {
        return id + ": " + nom + " " + prenom;
    }
    
    
}
