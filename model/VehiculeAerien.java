package model;

public class VehiculeAerien extends Vehicule{
    private float heuresVol;
    private int nbMoteurs;
    private String avion;

    public VehiculeAerien(int id,String marque,String etat,int vitesseMax,String model,float prixJour,int nbPlaces,float heuresVol,int nbMoteurs, String avion){
        super(id, marque, etat, vitesseMax, model, prixJour, nbPlaces);
        this.heuresVol = heuresVol;
        this.nbMoteurs = nbMoteurs;
        this.avion = avion;
    }
    /**
     * Obtient le nombre d'heures des vols faits par le vehicule aerien
     * @return le nombre d'heures des vols faits par le vehicule aerien
     */
    public float getHeuresVol() {
        return heuresVol;
    }
    /**
     * change le nombre d'heures du vol
     * @param heuresVol
     */
    public void setHeuresVol(float heuresVol) {
        this.heuresVol = heuresVol;
    }
    /**
     * Obtenir le nombre de moteurs du vehicule aerien 
     * @return le nombre de moteurs du vehicule aerien
     */
    public int getNbMoteurs() {
        return nbMoteurs;
    }
    
    /**
     * Permet de savoir le type d'avion choisi 
     * @return le type avion (cargo / plaisance)
     */
    public String getAvion() {
        return avion;
    }

    /**
     * Permet de changer un type d'avion
     * @param avion
     */
    public void setAvion(String avion) {
        this.avion = avion;
    }
    /**
     * Change le nombre de moteurs
     * @param nbMoteurs
     */
    public void setNbMoteurs(int nbMoteurs) {
        this.nbMoteurs = nbMoteurs;
    }
    @Override
    public String toString() {
        return "VehiculeAerien [id=" + id + ", heuresVol=" + heuresVol + ", marque=" + marque + ", nbMoteurs="
                + nbMoteurs + ", etat=" + etat + ", avion=" + avion + ", vitesseMax=" + vitesseMax + ", model=" + model
                + ", prixJour=" + prixJour + ", nbPlaces=" + nbPlaces + "]";
    }
    
    
}
