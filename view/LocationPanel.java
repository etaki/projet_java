package view;

import model.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LocationPanel extends JPanel{
    private JTextField dateDebutField;
    private JTextField dateFinField;
    private JComboBox<Integer> clientBox;
    private JComboBox<Integer> vehiculeBox1;
    private JLabel dateDebutLabel;
    private JLabel dateFinLabel;
    private JLabel clientLabel;
    private JLabel vehiculeLabel;
    private JButton locationButton;

    public LocationPanel(JComboBox<Integer> clientBoxP, JComboBox<Integer> vehiculeBox1P, JComboBox<Integer> locationBoxP){
        LocationPanel.this.setSize(500, 600);
        this.setLayout(new GridLayout(5, 2, 30, 30));
        

        this.dateDebutLabel = new JLabel("Date de début (j/m/a)");
        this.dateDebutField = new JTextField();
        this.dateFinLabel = new JLabel("Date de fin (j/m/a)");
        this.dateFinField = new JTextField();
        this.clientLabel = new JLabel("Client");
        this.clientBox = clientBoxP;
        this.vehiculeLabel = new JLabel("Véhicule");
        this.vehiculeBox1 = vehiculeBox1P;
        this.locationButton = new JButton("Faire une location");


        this.add(clientLabel);
        this.add(clientBox);
        this.add(vehiculeLabel);
        this.add(vehiculeBox1);
        this.add(dateDebutLabel);
        this.add(dateDebutField);
        this.add(dateFinLabel);
        this.add(dateFinField);
        this.add(locationButton);




        locationButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (clientBox.getSelectedItem().toString() == "" || vehiculeBox1.getSelectedItem().toString() == ""){
                    JOptionPane.showMessageDialog(LocationPanel.this, "Merci de sélectionner un client et un véhicule!", "Erreur", JOptionPane.ERROR_MESSAGE);
                }
                else{
                    Location location;
                    if(App.agence.getVehicule((int)vehiculeBox1.getSelectedItem()) instanceof VehiculeTerrestre){
                        location = new LocationVehiculeTerrestre(App.agence.getClient((int)clientBox.getSelectedItem()), App.agence.getVehicule((int)vehiculeBox1.getSelectedItem()), dateDebutField.getText(), dateFinField.getText(), 0, 0f, 0);
                    }
                    else{
                        location = new LocationVehiculeAerien(App.agence.getClient((int)clientBox.getSelectedItem()), App.agence.getVehicule((int)vehiculeBox1.getSelectedItem()), dateDebutField.getText(), dateFinField.getText(), 0, 0f, 0);
                    }
                    App.agence.addLocation(location);
                    App.agence.serialiserAgence();
                    clearAllFields();
                    locationBoxP.addItem(location.getId());
                    
                    JOptionPane.showMessageDialog(LocationPanel.this, "Location créée avec succès!", "Compte créé", JOptionPane.INFORMATION_MESSAGE);
                
                }
            }
        });
    }
    void clearAllFields(){
        dateDebutField.setText("");
        dateFinField.setText("");
    }
}
