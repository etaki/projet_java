package view;

import model.*;
import javax.swing.*;

public class App {
    private JFrame frame;
    private JPanel clientPanel;
    private JPanel locationPanel;
    private JTabbedPane tabbedPane;
    private CreerVehicule creerVehicule;
    private PrixPanel prixPanel;
    private ModifierVehicule modifierVehicule;
    public static JComboBox<Integer> clientBox;
    public static JComboBox<Integer> vehiculeBox1;
    public static JComboBox<Integer> vehiculeBox2;
    public static JComboBox<Integer> locationBox;
    public static Agence agence;

    public App(){
        agence = new Agence("null");
        agence.deserialiserAgence("./model/agence0.ag");
        
        frame = new JFrame();

        clientBox = new JComboBox<Integer>();
        vehiculeBox1 = new JComboBox<Integer>();
        vehiculeBox2 = new JComboBox<Integer>();
        locationBox = new JComboBox<Integer>();

        for(int i:agence.getClients().keySet()){
            clientBox.addItem(agence.getClient(i).getId());
            Client.nbClients++;
        }

        for(int i:agence.getVehicules().keySet()){
            vehiculeBox1.addItem(agence.getVehicule(i).getId());
            vehiculeBox2.addItem(agence.getVehicule(i).getId());
            Vehicule.nbVehicules++;
        }

        for(int i:agence.getLocations().keySet()){
            locationBox.addItem(agence.getLocation(i).getId());
            Location.nbLocations++;
        }

        creerVehicule = new CreerVehicule(vehiculeBox1, vehiculeBox2);
        clientPanel = new ClientPanel(clientBox);
        locationPanel = new LocationPanel(clientBox, vehiculeBox2, locationBox);
        modifierVehicule = new ModifierVehicule(vehiculeBox1, vehiculeBox2, locationBox);
        prixPanel = new PrixPanel(locationBox);
        tabbedPane = new JTabbedPane();

        
        frame.setBounds(700, 100, 650, 750);
        frame.setTitle("Gestionnaire de Location");
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        tabbedPane.addTab("Creer_Vehicule", creerVehicule);
        tabbedPane.addTab("Modifier_Vehicule", modifierVehicule);
        tabbedPane.addTab("Création de client", clientPanel);
        tabbedPane.addTab("Faire une location", locationPanel);
        tabbedPane.addTab("Calculer le prix", prixPanel);
        
        frame.add(tabbedPane);

    }
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new App();
            }
        });
    }
}
