package view;

import model.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
// import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridLayout;


public class ModifierVehicule extends JPanel{

    // private static ModifierVehicule modifierVehiculeInstance;  // Static Instanciation


    // panel 
    private JPanel firstPanel;
    private JPanel secondPanel;
    private JPanel thirdPanel;

    // Liste des Id des vehicules
    private JLabel listeVehicules;
    // private JComboBox<Integer> listeVehiculesBox;
    
    // Caracteristiques du vehicule
    public JLabel marque;
    public JTextField fieldMarque;
    public JLabel etat;
    public JTextField fieldEtat;
    public JLabel vitesseMax;
    public JTextField fieldVitesseMax;
    public JLabel modele;
    public JTextField fieldModele;
    public JLabel prixJour;
    public JTextField fieldPrixJour;
    public JLabel nbPlaces;
    public JTextField fieldNbPLaces;
    public JLabel kilometrage;
    public JTextField fieldKilometrage;
    public JLabel puissance;
    public JTextField fieldPuissance;
    public JLabel forfaitKilometrique;
    public JTextField fieldForfait;
    public JLabel toitOuvrant;
    public JCheckBox isToitOuvrant;
    public JLabel carenage;
    public JCheckBox isCarenage;
    public JLabel heuresVol;
    public JTextField fieldHeuresVol;
    public JLabel nbMoteurs;
    public JTextField fieldNbMoteurs;
    public JLabel typeAvion;
    public JComboBox<Avion> typeAvionBox;

    // button: creeVehicule
    JButton buttonValider = new JButton("Valider");
    JButton buttonSupprimer = new JButton("Supprimer");
    
    // Constructeur
    public ModifierVehicule(JComboBox<Integer> listeVehiculesId,JComboBox<Integer> listeVehiculesId2, JComboBox<Integer> locationBoxP){
        setLayout(new GridLayout(19,2,2,10));
        // panel
        firstPanel = new JPanel(new GridLayout(1,2,2,10));
        secondPanel = new JPanel(new GridLayout(14, 2, 2, 10));
        thirdPanel = new JPanel(new GridLayout(3, 2, 2, 10));
        // Layouts
        listeVehicules = new JLabel("Liste des vehicules");
        // listeVehiculesBox = listeVehiculesId; // comboBox


        marque = new JLabel("Marque: ");
        fieldMarque = new JTextField();
        etat = new JLabel("Etat: ");
        fieldEtat = new JTextField();
        vitesseMax = new JLabel("Vitesse Max: ");
        fieldVitesseMax = new JTextField();
        modele = new JLabel("Modele: ");
        fieldModele = new JTextField();
        prixJour = new JLabel("Prix par jour: ");
        fieldPrixJour = new JTextField();
        nbPlaces = new JLabel("Nombre de places: ");
        fieldNbPLaces = new JTextField();
        kilometrage = new JLabel("Kilometrage: ");
        fieldKilometrage = new JTextField();
        puissance = new JLabel("Puissance: ");
        fieldPuissance = new JTextField();
        forfaitKilometrique = new JLabel("Forfait kilometrique: ");
        fieldForfait = new JTextField();
        toitOuvrant = new JLabel("Toit Ouvrant: ");
        isToitOuvrant = new JCheckBox();
        carenage = new JLabel("Carenage: ");
        isCarenage = new JCheckBox();
        heuresVol = new JLabel("Heures de vol: ");
        fieldHeuresVol = new JTextField();
        nbMoteurs = new JLabel("Nombre de moteurs: ");
        fieldNbMoteurs = new JTextField();
        typeAvion = new JLabel("Type d'avion: ");
        typeAvionBox = new JComboBox<Avion>(Avion.values());
        creerLayouts(listeVehiculesId);
        // Set layout for frame
        setLayout(new BorderLayout());
        // add panels in frame
        add(firstPanel, BorderLayout.NORTH);
        add(secondPanel, BorderLayout.CENTER);
        add(thirdPanel, BorderLayout.SOUTH);
        
        // creerListener
        creerListener(listeVehiculesId, listeVehiculesId2, locationBoxP);
    }   

    // Methode du singleton
       /* public static ModifierVehicule getModifierVehiculeInstance(JComboBox<Integer> listeVehiculesId) {
        if (modifierVehiculeInstance == null) {
            modifierVehiculeInstance = new ModifierVehicule(listeVehiculesId);
        }
        return modifierVehiculeInstance;
    }
 */
    /**
     * Permet de basculer vers le panel de Modifier Vehicule 
     * en cliquant su le bouton convenable 
     * @return le bouton 
     */
    /* public JButton getTabButton() {
        JButton button = new JButton("Creer_Vehicule");
        button.addActionListener(e -> {
            App mainApp = (App)SwingUtilities.getWindowAncestor(this);
            mainApp.showModifierVehicule();;
        });
        return button;
    } */

    /**
     * Permet de crer les Layouts et les ajouter 
     * dans diffents panels
     */
    public void creerLayouts(JComboBox<Integer> listeVehiculesId) { // ajouter les attributs dans les panels
        // add Layputs in panel
        firstPanel.add(listeVehicules);
        firstPanel.add(listeVehiculesId);

        secondPanel.add(marque);
        secondPanel.add(fieldMarque);
        secondPanel.add(etat);
        secondPanel.add(fieldEtat);
        secondPanel.add(vitesseMax);
        secondPanel.add(fieldVitesseMax);
        secondPanel.add(modele);
        secondPanel.add(fieldModele);
        secondPanel.add(prixJour);
        secondPanel.add(fieldPrixJour);
        secondPanel.add(nbPlaces);
        secondPanel.add(fieldNbPLaces);
        secondPanel.add(kilometrage);
        secondPanel.add(fieldKilometrage);
        secondPanel.add(puissance);
        secondPanel.add(fieldPuissance);
        secondPanel.add(forfaitKilometrique);
        secondPanel.add(fieldForfait);
        secondPanel.add(heuresVol);
        secondPanel.add(fieldHeuresVol);
        secondPanel.add(nbMoteurs);
        secondPanel.add(fieldNbMoteurs);
        secondPanel.add(typeAvion);
        secondPanel.add(typeAvionBox);

        thirdPanel.add(toitOuvrant);
        thirdPanel.add(isToitOuvrant);
        thirdPanel.add(carenage);
        thirdPanel.add(isCarenage);
        thirdPanel.add(buttonValider);
        thirdPanel.add(buttonSupprimer);

        firstPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 20));
        secondPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 0, 20));
        thirdPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 20));
    }
    
    

    /**
     * Cette fonction permet pour chaque id selectionne d'afficher
     * les caractersitiques du vehicule correspondant, avec la 
     * possibilite de supprimer le vehicule ou modifier ses 
     * caractersitiques
     */
    public void creerListener(JComboBox<Integer> listeVehiculesId, JComboBox<Integer> listeVehiculesId2, JComboBox<Integer> locationBoxP){

        // valider les modifications
        buttonValider.addActionListener(new ActionListener() { // Enregistrer le smodifications dans la liste convenable
            
            public void actionPerformed(ActionEvent e){
                Integer vehiculeId =  (Integer)listeVehiculesId.getSelectedItem();
                Vehicule vehicule =  App.agence.getVehicule(vehiculeId);
                Vehicule.nbVehicules--;
                if(vehicule instanceof Voiture){
                    Voiture voiture = new Voiture(vehiculeId, fieldMarque.getText(),
                                                fieldEtat.getText(), Integer.parseInt(fieldVitesseMax.getText()),
                                                fieldModele.getText(), Float.parseFloat(fieldPrixJour.getText()), 
                                                Integer.parseInt(fieldNbPLaces.getText()), Integer.parseInt(fieldKilometrage.getText()),
                                                Integer.parseInt(fieldPuissance.getText()), Integer.parseInt(fieldForfait.getText()),
                                                isToitOuvrant.isSelected());
                    App.agence.modifVehicule(vehiculeId, voiture);

                }else if(vehicule instanceof Moto){

                    Moto moto = new Moto(vehiculeId, fieldMarque.getText(),
                                                fieldEtat.getText(), Integer.parseInt(fieldVitesseMax.getText()),
                                                fieldModele.getText(), Float.parseFloat(fieldPrixJour.getText()), 
                                                Integer.parseInt(fieldNbPLaces.getText()), Integer.parseInt(fieldKilometrage.getText()),
                                                Integer.parseInt(fieldPuissance.getText()), Integer.parseInt(fieldForfait.getText()),
                                                isCarenage.isSelected());
                    App.agence.modifVehicule(vehiculeId, moto);

                }else if(vehicule instanceof VehiculeAerien){

                    VehiculeAerien vehiculeAerien = new VehiculeAerien(vehiculeId, fieldMarque.getText(),
                                                fieldEtat.getText(), Integer.parseInt(fieldVitesseMax.getText()),
                                                fieldModele.getText(), Float.parseFloat(fieldPrixJour.getText()), 
                                                Integer.parseInt(fieldNbPLaces.getText()), Float.parseFloat(fieldHeuresVol.getText()),
                                                Integer.parseInt(fieldNbMoteurs.getText()), String.valueOf(typeAvionBox.getSelectedItem()));
                    App.agence.modifVehicule(vehiculeId, vehiculeAerien);
                }
                App.agence.serialiserAgence();                
            }
        });
        // Supprimer Id
        buttonSupprimer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                int selectedItem = (int)listeVehiculesId.getSelectedItem();
                App.agence.delVehicule(selectedItem);
                listeVehiculesId.removeItem(selectedItem);
                listeVehiculesId2.removeItem(selectedItem);
                clearAllFields(); // Nettoyer les JTextFields
                App.agence.serialiserAgence(); 
            }
        });

        listeVehiculesId.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){           
                ajouterDonneesModifiables(listeVehiculesId);
            }
        });
    }

    /**
     * cette fonction permet de renitialiser les champs deja remplies
     */

    public void clearAllFields(){
        fieldMarque.setText("");
        fieldEtat.setText("");
        fieldVitesseMax.setText("");
        fieldModele.setText("");
        fieldPrixJour.setText("");
        fieldNbPLaces.setText("");
        fieldKilometrage.setText("");
        fieldPuissance.setText("");
        fieldForfait.setText("");
        fieldHeuresVol.setText("");
        fieldNbMoteurs.setText("");
    }

    /**
     * Ajoute dans les TextField les donnees 
     * de l'id du vehicule selectionne 
     */

     public void ajouterDonneesModifiables(JComboBox<Integer> listeVehiculesId){
        if(listeVehiculesId.getSelectedItem() != null){

            int vehiculeId =  (int)listeVehiculesId.getSelectedItem();
            Vehicule vehicule =  App.agence.getVehicule(vehiculeId);
            
            fieldMarque.setText(String.valueOf(vehicule.getMarque()));
            fieldEtat.setText(vehicule.getEtat());
            fieldVitesseMax.setText(String.valueOf(vehicule.getVitesseMax()));
            fieldModele.setText(vehicule.getModel());
            fieldPrixJour.setText(String.valueOf(vehicule.getPrixJour()));
            fieldNbPLaces.setText(String.valueOf(vehicule.getNbPlaces()));

            if(vehicule instanceof VehiculeTerrestre){ // Si l'id coreespond a un vehicule terrestre
                VehiculeTerrestre vehiculeTerrestre = (VehiculeTerrestre) vehicule;
                fieldHeuresVol.setEnabled(false);
                fieldNbMoteurs.setEnabled(false);
                typeAvionBox.setEnabled(false);
                
                fieldKilometrage.setEnabled(true);
                fieldPuissance.setEnabled(true);
                fieldForfait.setEnabled(true);

                fieldKilometrage.setText(String.valueOf(vehiculeTerrestre.getKilometrage()));
                fieldPuissance.setText(String.valueOf(vehiculeTerrestre.getPuissance()));
                fieldForfait.setText(String.valueOf(vehiculeTerrestre.getForfaitKilometrique()));
                
                if(vehiculeTerrestre instanceof Voiture){
                    Voiture voiture = (Voiture) vehiculeTerrestre;

                    carenage.setEnabled(false);
                    isCarenage.setEnabled(false);
                    isToitOuvrant.setEnabled(true);
                    isToitOuvrant.setSelected(voiture.isToitOuvrant());
                    
                }else if(vehiculeTerrestre instanceof Moto){
                    Moto moto = (Moto) vehiculeTerrestre;

                    toitOuvrant.setEnabled(false);
                    isToitOuvrant.setEnabled(false);
                    isCarenage.setEnabled(true);
                    isCarenage.setSelected(moto.isCarenage());
                    
                }
            }else if(vehicule instanceof VehiculeAerien){ // Si l'id coreespond a un vehicule aerien

                VehiculeAerien vehiculeAerien = (VehiculeAerien) vehicule;
                fieldKilometrage.setEnabled(false);
                fieldPuissance.setEnabled(false);
                fieldForfait.setEnabled(false);

                fieldHeuresVol.setEnabled(true);
                fieldNbMoteurs.setEnabled(true);
                typeAvionBox.setEnabled(true);
            
                fieldHeuresVol.setText(String.valueOf(vehiculeAerien.getHeuresVol()));
                fieldNbMoteurs.setText(String.valueOf(vehiculeAerien.getHeuresVol()));
            
            }
        }           
    }
}


