package view;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridLayout;
import java.awt.Label;
import model.*;


public class PrixPanel extends JPanel{
    
    public JLabel idLocation;
    private JComboBox<Integer> locationBox;
    public JLabel nbKilometres;
    public JTextField nbKilometresField;
    public JLabel nbHeuresVol;
    public JTextField nbHeuresField;
    public JLabel prixFinal;
    public Label prixFinalLabel;
    private JButton calculerPrixButton;
    
    public PrixPanel(JComboBox<Integer> locationBoxP) {
        this.locationBox = locationBoxP;
        idLocation = new JLabel("Ids de location: ");
        nbKilometres = new JLabel("Nombre de kilometres: ");
        nbKilometresField = new JTextField();
        nbHeuresVol = new JLabel("Nombre d'heures de vol: ");
        nbHeuresField = new JTextField();
        prixFinal = new JLabel("Prix final:");
        prixFinalLabel = new Label();
        calculerPrixButton = new JButton("Calculer le prix");
        
        creerLayouts();
        creerListener();
    }

    /**
     * Elle permet d'ajouter les layouts dans la Jpanel
     */
    public void creerLayouts() {
        add(idLocation);
        add(this.locationBox);
        add(nbKilometres);
        add(nbKilometresField);
        add(nbHeuresVol);
        add(nbHeuresField);
        add(prixFinal);
        add(prixFinalLabel);
        add(calculerPrixButton);
        
    }

    /**
     * Elle permet de definir tous les actions a faire
     * en cas de clique sur un bouton ou en cas de selection
     * d'un id dans la comboBox
     */
    public void creerListener() {
        this.setLayout(new GridLayout(5,2,2,10));
        this.setBorder(BorderFactory.createEmptyBorder(80, 20, 80, 20));
        
        this.locationBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Location location = App.agence.getLocation((int)locationBox.getSelectedItem());
                if(location instanceof LocationVehiculeTerrestre) {
                    nbKilometres.setEnabled(true);
                    nbKilometresField.setEnabled(true);
                    nbHeuresVol.setEnabled(false);
                    nbHeuresField.setEnabled(false);
                }else if(location instanceof LocationVehiculeAerien) {
                    nbHeuresVol.setEnabled(true);
                    nbHeuresField.setEnabled(true);
                    nbKilometres.setEnabled(false);
                    nbKilometresField.setEnabled(false);
                }
            }
        });

        calculerPrixButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Location location = App.agence.getLocation((int)locationBox.getSelectedItem());
                if(location instanceof LocationVehiculeTerrestre) {
                    LocationVehiculeTerrestre LocationVehiculeTerrestre = (LocationVehiculeTerrestre) location;
                    Float clientKilometres = Float.parseFloat(nbKilometresField.getText());
                    Float prix = LocationVehiculeTerrestre.rendreVehicule(clientKilometres);
                    prixFinalLabel.setText(String.valueOf(prix));
                
                }else if(location instanceof LocationVehiculeAerien) {
                    LocationVehiculeAerien locationVehiculeAerien = (LocationVehiculeAerien) location;
                    Float clientHeuresVol = Float.parseFloat(nbHeuresField.getText());
                    Float prix = locationVehiculeAerien.rendreVehicule(clientHeuresVol);
                    prixFinalLabel.setText(String.valueOf(prix));
                }
            }
            });
        }
    }
