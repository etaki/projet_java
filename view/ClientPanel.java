package view;

import model.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class ClientPanel extends JPanel {
    private JLabel nom;
    private JLabel prenom;
    private JLabel adresse;
    private JTextField nomField;
    private JTextField prenomField;
    private JTextField adresseField;
    private JButton creationButton;
    

    /**
     * Crée un panel qui permet de créer un nouveau client
     */
    public ClientPanel(JComboBox<Integer> clientBoxP){
        ClientPanel.this.setSize(500, 300);
        this.setLayout(new GridLayout(5, 2, 70, 70));

        nom = new JLabel("Nom");
        prenom = new JLabel("Prénom");
        adresse = new JLabel("Adresse");

        nomField = new JTextField();
        prenomField = new JTextField();
        adresseField = new JTextField();

        creationButton = new JButton("Créer un client");

        creationButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (nomField.getText().equals("") || prenomField.getText().equals("") || adresseField.getText().equals("")){
                    JOptionPane.showMessageDialog(ClientPanel.this, "Tous les champs doivent être remplis!", "Erreur", JOptionPane.ERROR_MESSAGE);
                }
                else{
                    Client client = new Client(nomField.getText(), prenomField.getText(), adresse.getText(), (new Date()).toString());
                    App.agence.addClient(client);
                    App.agence.serialiserAgence();
                    
                    clientBoxP.addItem(client.getId());
                    clearAllFields();
                    JOptionPane.showMessageDialog(ClientPanel.this, "Client créé avec succès!", "Compte créé", JOptionPane.INFORMATION_MESSAGE);
                
                }
            }
        });

        this.add(nom);
        this.add(nomField);
        this.add(prenom);
        this.add(prenomField);
        this.add(adresse);
        this.add(adresseField);
        this.add(creationButton);

    }

    void clearAllFields(){
        prenomField.setText("");
        nomField.setText("");
        adresseField.setText("");
    }
}

