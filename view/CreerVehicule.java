package view;

import model.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
// import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.GridLayout;


public class CreerVehicule  extends JPanel implements Runnable{
    public ModifierVehicule modifierVehicule;
    // frame 
    JFrame frame;
    // comboBox
    private JComboBox<Integer> vehiculeBox1;
    private JComboBox<Integer> vehiculeBox2;

    
    // TYpes JComboBox
    public JLabel typesVehicules;
    public JComboBox<String> vehiculesBox; // Terrestre ou Aerien
    
    // Si vehicule terrestre;
    public JLabel typeVehiculeTerrestre;
    public JComboBox<String> vehiculeTerrestreBox; // JComboBox Voiture / Moto
    public JLabel kilometrage;
    public JTextField fieldKilometrage;
    public JLabel puissance;
    public JTextField fieldPuissance;
    public JLabel forfaitKilometrique;
    public JTextField fieldForfaitKilometrique;
    // Si Voiture
    public JLabel toitOuvrant;
    public JCheckBox isToitOuvrant;
    // Si Moto
    public JLabel carenage;
    public JCheckBox isCarenage;


    // Si Vehicule Aerien; setText automatiquement: Avion
    public JLabel typeVehiculeAerien;
    public JComboBox<String> vehiculeAerienBox; // Avion

    // Types Avion
    public JLabel typeAvion;
    public JComboBox<String> typeAvionBox; // Cargo / Plaisance 
    public JLabel heuresVol;
    public JTextField fieldHeuresVol;
    public JLabel nbMoteurs;
    public JTextField fieldNbMoteurs;

    // Caracteristiques du vehicule
    public JLabel marque;
    public JTextField fieldMarque;
    public JLabel etat;
    public JTextField fieldEtat;
    public JLabel vitesseMax;
    public JTextField fieldVitesseMax;
    public JLabel modele;
    public JTextField fieldModele;
    public JLabel prixJour;
    public JTextField fieldPrixJour;
    public JLabel nbPlaces;
    public JTextField fieldNbPLaces;


    // button: creeVehicule
    JButton buttonCreer = new JButton("Creer un nouveau vehicule");
    // Listes
    String[] listeVehicules = {"Terrestre", "Aerien"};
    String[] listeVehiculeTerrestre = {"Voiture", "Moto"};
    String[] listeVehiculeAerien = {"Avion"};
    String[] listeAvion = {"Cargo", "PLaisance"};
    
    // Constructeur
    public CreerVehicule(JComboBox<Integer> vehiculeBox1P, JComboBox<Integer> vehiculeBox2P){
        // modifierVehicule instance
        // this.modifierVehicule = ModifierVehicule.getModifierVehiculeInstance(vehiculeBox1); // Insance de ModifierVehicule

        this.vehiculeBox1 =  vehiculeBox1P;
        this.vehiculeBox2 =  vehiculeBox2P;

        this.setLayout(new GridLayout(18,2,2,10));
        this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));        
 
        typesVehicules = new JLabel("Type du vehicule: ");
        vehiculesBox = new JComboBox<String>(listeVehicules); // Terrestre / Aerien
        
        // Vehicule Terrestre
        typeVehiculeTerrestre = new JLabel("Type du vehicule terrestre: ");
        vehiculeTerrestreBox = new JComboBox<String>(listeVehiculeTerrestre); // Voiture / Moto
        kilometrage = new JLabel("Kilometrage: ");
        fieldKilometrage = new JTextField();
        puissance = new JLabel("Puissance: ");
        fieldPuissance = new JTextField();
        forfaitKilometrique = new JLabel("Forfait Kilometrique: ");
        fieldForfaitKilometrique = new JTextField();
        // Voiture
        toitOuvrant = new JLabel("Toit Ouvrant: ");
        isToitOuvrant = new JCheckBox();
        // Moto
        carenage = new JLabel("Carenage: ");
        isCarenage  = new JCheckBox();
        carenage.setEnabled(false);
        isCarenage.setEnabled(false);
        
        // Vehicule Aerien
        typeVehiculeAerien = new JLabel("Type du vehicule aerien: ");
        vehiculeAerienBox = new JComboBox<String>(listeVehiculeAerien); // Avion
        typeVehiculeAerien.setEnabled(false);
        vehiculeAerienBox.setEnabled(false);
        // Type Avion
        typeAvion = new JLabel("Type d'avion: ");
        typeAvionBox = new JComboBox<String>(listeAvion); // Cargo / PLaisance
        typeAvion.setEnabled(false);
        typeAvionBox.setEnabled(false);
        heuresVol = new JLabel("Nombre d'heures de vol: ");
        fieldHeuresVol = new JTextField();
        heuresVol.setEnabled(false);
        fieldHeuresVol.setEnabled(false);
        nbMoteurs = new JLabel("Nombre de moteurs");
        fieldNbMoteurs = new JTextField();
        nbMoteurs.setEnabled(false);
        fieldNbMoteurs.setEnabled(false);

        marque = new JLabel("Marque: ");
        fieldMarque = new JTextField();
        etat = new JLabel("Etat: ");
        fieldEtat = new JTextField();
        vitesseMax = new JLabel("Vitesse Max: ");
        fieldVitesseMax = new JTextField();
        modele = new JLabel("Modele: ");
        fieldModele = new JTextField();
        prixJour = new JLabel("Prix par jour: ");
        fieldPrixJour = new JTextField();
        nbPlaces = new JLabel("Nombre de places: ");
        fieldNbPLaces = new JTextField();
        // add in panel
        //getTabButton();
        creerLayouts();
        creerListeners();
    }   
    /**
     * Ajouter tous les composants dans le panel
     */
    public void creerLayouts(){
        add(typesVehicules);
        add(vehiculesBox);
        add(typeVehiculeTerrestre);
        add(vehiculeTerrestreBox);
        add(kilometrage);
        add(fieldKilometrage);
        add(puissance);
        add(fieldPuissance);
        add(forfaitKilometrique);
        add(fieldForfaitKilometrique);
        add(toitOuvrant);
        add(isToitOuvrant);
        add(carenage);
        add(isCarenage);
        add(typeVehiculeAerien);
        add(vehiculeAerienBox);
        add(typeAvion);
        add(typeAvionBox);
        add(heuresVol);
        add(fieldHeuresVol);
        add(nbMoteurs);
        add(fieldNbMoteurs);
        add(marque);
        add(fieldMarque);
        add(etat);
        add(fieldEtat);
        add(vitesseMax);
        add(fieldVitesseMax);
        add(modele);
        add(fieldModele);
        add(prixJour);
        add(fieldPrixJour);
        add(nbPlaces);
        add(fieldNbPLaces);
        add(buttonCreer);
    }

    /**
     * Definit les actionListener
     */
    public void creerListeners(){
        
        vehiculesBox.addActionListener(new ActionListener(){ // Action Listener pour la comboBox (VehiculeTerrestre / VehiculeAeruen) 
            public void actionPerformed(ActionEvent e) {
                String selectedItem = vehiculesBox.getSelectedItem().toString();
                if(selectedItem.equals("Terrestre")){
                    typeVehiculeAerien.setEnabled(false);
                    vehiculeAerienBox.setEnabled(false);
                    typeAvion.setEnabled(false);
                    typeAvionBox.setEnabled(false);
                    heuresVol.setEnabled(false);
                    fieldHeuresVol.setEnabled(false);
                    nbMoteurs.setEnabled(false);
                    fieldNbMoteurs.setEnabled(false);

                    typeVehiculeTerrestre.setEnabled(true); // Si on selectionne VehiculeTerrestre
                    vehiculeTerrestreBox.setEnabled(true);
                    kilometrage.setEnabled(true);
                    fieldKilometrage.setEnabled(true);
                    puissance.setEnabled(true);
                    fieldPuissance.setEnabled(true);
                    forfaitKilometrique.setEnabled(true);
                    fieldForfaitKilometrique.setEnabled(true);
                    if(vehiculeTerrestreBox.getSelectedItem().toString().equals("Voiture")){
                        carenage.setEnabled(false);
                        isCarenage.setEnabled(false);
                        toitOuvrant.setEnabled(true);
                        isToitOuvrant.setEnabled(true);
                    }else{
                        carenage.setEnabled(true);
                        isCarenage.setEnabled(true);
                        toitOuvrant.setEnabled(false);
                        isToitOuvrant.setEnabled(false);
                    }

                } else if(selectedItem.equals("Aerien")) { // Si on selectionne VehiculeAerien
                    typeVehiculeAerien.setEnabled(true);
                    vehiculeAerienBox.setEnabled(true);
                    typeAvion.setEnabled(true);
                    typeAvionBox.setEnabled(true);
                    heuresVol.setEnabled(true);
                    fieldHeuresVol.setEnabled(true);
                    nbMoteurs.setEnabled(true);
                    fieldNbMoteurs.setEnabled(true);

                    typeVehiculeTerrestre.setEnabled(false);
                    vehiculeTerrestreBox.setEnabled(false);
                    carenage.setEnabled(false);
                    isCarenage.setEnabled(false);
                    toitOuvrant.setEnabled(false);;
                    isToitOuvrant.setEnabled(false);
                    kilometrage.setEnabled(false);
                    fieldKilometrage.setEnabled(false);
                    puissance.setEnabled(false);
                    fieldPuissance.setEnabled(false);
                    forfaitKilometrique.setEnabled(false);
                    fieldForfaitKilometrique.setEnabled(false);
                }
            }
        });

        vehiculeTerrestreBox.addActionListener(new ActionListener() { // ActionListener pour la comboBox (Voiture / Moto)
            public void actionPerformed(ActionEvent e) {
                String selectedItem = vehiculeTerrestreBox.getSelectedItem().toString();
                if(selectedItem.equals("Voiture")){             
                    carenage.setEnabled(false);
                    isCarenage.setEnabled(false);
                    toitOuvrant.setEnabled(true);
                    isToitOuvrant.setEnabled(true);
                
                }else if(selectedItem.equals("Moto")){
                    carenage.setEnabled(true);
                    isCarenage.setEnabled(true);
                    toitOuvrant.setEnabled(false);;
                    isToitOuvrant.setEnabled(false);
                }
            }
        });

        buttonCreer.addActionListener(new ActionListener() { // ActionListener pour creer un nouveau vehicule
            public void actionPerformed(ActionEvent e) {
                JTextField[] listeFields = {fieldMarque, fieldEtat,
                    fieldVitesseMax, fieldModele, fieldPrixJour, fieldNbPLaces, fieldHeuresVol,
                    fieldNbMoteurs, fieldKilometrage, fieldPuissance, fieldForfaitKilometrique};
        
                // Check if all fields are filled
                boolean allFieldsFilled = true;
                for (JTextField field : listeFields){
                    if (field.isEnabled() && field.getText().isEmpty()){
                        allFieldsFilled = false;
                        break;
                    }
                }
        
                if (allFieldsFilled){
                    // Show confirmation dialog
                    int result = JOptionPane.showConfirmDialog(null, "Confirmer la création du véhicule ?", 
                                    "Confirmation", JOptionPane.OK_CANCEL_OPTION);
                    if (result == JOptionPane.OK_OPTION){
                        ajouterInMap(); // ajouter nouveau vehicule a la liste
                        clearAllFields();
                    }
                } else {
                    afficherWarning();
                }
            }
        });
    }

    /**
     * Nettoyer tous les fields apres avoir 
     * remplir tous les champs et valider 
     */

     public void clearAllFields(){
        fieldMarque.setText("");
        fieldEtat.setText("");
        fieldVitesseMax.setText("");
        fieldModele.setText("");
        fieldPrixJour.setText("");
        fieldNbPLaces.setText("");
        fieldKilometrage.setText("");
        fieldPuissance.setText("");
        fieldForfaitKilometrique.setText("");
        fieldHeuresVol.setText("");
        fieldNbMoteurs.setText("");
    }

    /**
     * Ajouter le nouveau vehicule dans la HashMap
     */
    public void ajouterInMap() {
        String idText = String.valueOf(Vehicule.nbVehicules);
    
        // Check if idText is not empty
        if (!idText.isEmpty()) {
            try {
                int id = Integer.parseInt(idText);
                String marque = fieldMarque.getText();
                String etat = fieldEtat.getText();
                int vitesseMax = Integer.parseInt(fieldVitesseMax.getText().trim());
                String modele = fieldModele.getText();
                float prixJour = Float.parseFloat(fieldPrixJour.getText());
                int nbPlaces = Integer.parseInt(fieldNbPLaces.getText().trim());
    
                // Si c'est un Vehicule Terrestre
                if (vehiculesBox.getSelectedItem().toString().equals("Terrestre")) {

                    int kilometrage = Integer.parseInt(fieldKilometrage.getText().trim());
                    int puissance = Integer.parseInt(fieldPuissance.getText().trim());
                    int forfaitKilometrique = Integer.parseInt(fieldForfaitKilometrique.getText().trim());
                    // Si c'est une voiture
                    if (vehiculeTerrestreBox.getSelectedItem().toString().equals("Voiture")) {
                        Voiture voiture = new Voiture(id, marque, etat, vitesseMax,
                                modele, prixJour, nbPlaces, kilometrage, puissance,
                                forfaitKilometrique, isToitOuvrant.isSelected());
                        
                        App.agence.addVehicule(voiture);
                    
                    } else if (vehiculeTerrestreBox.getSelectedItem().toString().equals("Moto")) {
                        // Si c'est une moto
                        Moto moto = new Moto(id, marque, etat, vitesseMax,
                                modele, prixJour, nbPlaces, kilometrage, puissance,
                                forfaitKilometrique, isCarenage.isSelected());
                        
                        App.agence.addVehicule(moto);
                    }
                
                // Si c'est un Vehicule Aerien
                } else if (vehiculesBox.getSelectedItem().toString().equals("Aerien")) {

                    float heuresVol = Float.parseFloat(fieldHeuresVol.getText());
                    int nbMoteurs = Integer.parseInt(fieldNbMoteurs.getText().trim());
                    String avion = String.valueOf(typeAvionBox.getSelectedItem());

                    VehiculeAerien vehiculeAerien = new VehiculeAerien(id, marque, etat, vitesseMax,
                            modele, prixJour, nbPlaces, heuresVol, nbMoteurs, avion);
                    
                    App.agence.addVehicule(vehiculeAerien);
                } 
                System.out.println("Listes des vehicules:  " +  App.agence.getVehicules());  
                // Ajouter dans la comboBox de la classe ModifierVehicule
                vehiculeBox1.addItem(App.agence.getVehicules().entrySet().stream().reduce((one, two) -> two).get().getValue().getId());
                vehiculeBox2.addItem(App.agence.getVehicules().entrySet().stream().reduce((one, two) -> two).get().getValue().getId()); // Ajouter dans la HashMap l'id du dernier vehicule ajoutee
                App.agence.serialiserAgence();
            } catch (NumberFormatException e) {
                System.err.println("Error parsing input: " + e.getMessage());
            }
        } else {
            System.err.println("Le champ Id est vide !");
        }
    }

    /**
     * Envoie un message si l'un des champs n'est pas rempli
     */
    public void afficherWarning(){
        JOptionPane.showMessageDialog(frame, "Un champ est vide !", null, JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public void run() {
        new CreerVehicule(vehiculeBox1, vehiculeBox2);
    }
}
